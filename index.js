/*alert("HELLO");*/

// A VARIABLE WITH MULTIPLE VALUES
		// a : array
		//
// JS OBJECT
	// like array , it contains multiple values
	// unlike an array

	// STRUCTURE 
		/*
				let objectName = {
	
					key : value;
					key : (function)
					key : object {
									
							key : value
					}
				}
	
		*/

	let cellphone = {
			name : "Nokia 3210",
			manufactureDate : 1999


	};

	console.log(cellphone);
	console.log(typeof cellphone);

//CREATING OBJECTS USING A CONSTRUCTOR FUNCTION
		/*
			SYNTAX : 

				function ObjectName(keyA, keyB){
		
						this.keyA = keyA;
						this.keyB = keyB;
				}
		
		*/

		function Laptop(name, manufactureDate) {
					this.name = name;
					this.releaseDate = manufactureDate

		};

		let laptop = new Laptop("Lenovo", 2008);
		console.log(laptop);
					// will create new object
					// new operator creates an instance of an object


		let laptop1 = Laptop("Asus", 2010);
		console.log(laptop1);
					// results :  undefined



// CREATING EMPTY OBJECTS
		let computer = {};
		let myComputer = new Object();
		console.log(computer);
		console.log(myComputer);

// ACCESSING ARRAY OBJECTS
		let machines = [laptop, laptop1];

		// access the property of an object inside an array
		console.log(machines[0].name);
					// result : Lenovo
			//another way of accessing properties in an array
		console.log(machines[0]["releaseDate"]);
					// result : 2008

// INITIALIZING / ADDING / DELETING / REASSIGNING OBJECT PROPERTIES

		let car = {};

		// initializing/adding object properties using dot nototation

		car.name = "Honda Civic";
		console.log(car);

		//another way of adding object property with square brackets
		car["manufactureDate"] = 2019;
		console.log(car);

		//deleting object properties
		delete car["name"];
		console.log(car);

		/*MINI ACTIVITY
			add properties to the object car */

		car.brand = "Nissan";
		car.model = "Almera";
		car.color = "black";
		car.location = "Cagayan De Oro";
		console.log(car);

		//Reassigning object properties
		car.manufactureDate = 2016;


// OBJECT METHODS
		// a method is a function which is a property of an object
		//they are also functions and one of the key differences they have is that methods
		// are functions related to a specific object
		// methods are defined based on what an object is capable of doing and how it should works

		 let person = {
		 	name : "Jin",
		 	talk : function(){
		 			console.log(`Hello my name is ${this.name}`)
		 	
		 }
		};
		 person.talk();

		 let friend = {
		 	firstName : "Bam",
		 	lastName : "Jeon",
		 	address : {
		 		city : "Caloocan",
		 		country : "Philippines"
		 	},
		 	emails : ["bam@mail.com", "jeonbam@gmail.com"],
		 	introduce : function(){
		 		console.log(`Hello my name is ${this.firstName} ${this.lastName},I live in ${this.address.city}. My email is ${this.emails[0]}`)
		 	}
		 };

		 friend.introduce();


		 // REAL WORLD APPLICATION
			// SCENARIO:
				// 1. Create a game that would have several pokemns to interact with each other
				// 2. Every pokemen should have stats, properties, functions

			let myPokemen = {
				name : "Bulbasaur",
				level : 3,
				health : 100,
				attack : 50,
				tackle : function(){
					console.log(`${this.name} tackled another pokemon`)
					console.log(`targetPokemen's health is now reduced`)
				},
				faint: function (){
					console.log(`${this.name} fainted`)
				}

			};

			myPokemen.tackle();
			myPokemen.faint();

			// Creating object with an object constructor

			function Pokemon(name, lvl, hp){
					//Properties
					this.name = name;
					this.lvl = lvl;
					this.health = hp * 2 ;
					this.attack = lvl;

					// methods
					this.tackle = function(target){
						console.log(`${this.name} tackled ${target.name}`);
						console.log(`targetPokemen's health is now reduced`);
						console.log(target.name);
						console.log(this.name);
					};
					this.faint = function(){
						console.log(`${this.name} fainted`)
					}
			};

			// create new instances of the Pokemo object each with their unique properties

			let pikachu = new Pokemon("Pikachu", 3, 50);
			let ratata = new Pokemon("Ratata", 5, 25);

			// Providing the "ratata" object as an argument to "pikachu" tackle method
			//will creatE interaction between the two objects:

			pikachu.tackle(ratata);
			ratata.tackle(pikachu);



